Spring Boot各种日志记录方式详解

1、日志框架比较（slf4j、log4j、logback、log4j2）

Spring Boot在所有内部日志中使用Commons Logging，但是默认配置也提供了对常用日志的支持，
如：Java Util Logging，Log4J, Log4J2和Logback。每种Logger都可以通过配置使用控制台或者文件输出日志内容。

1.1 slf4j

slf4j是对所有日志框架制定的一种规范、标准、接口，并不是一个框架的具体的实现，因为接口并不能独立使用，需要和具体的日志框架实现配合使用（如log4j、logback）
