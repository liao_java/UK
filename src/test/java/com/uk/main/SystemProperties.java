package com.uk.main;

import java.util.Properties;

public class SystemProperties {

    // 系统属性
    public static void main(String[] args) {

        Properties properties = System.getProperties();

        // java.version Java 运行时环境版本
        // java.vendor Java 运行时环境供应商
        // java.vendor.url Java 供应商的 URL
        // os.name 操作系统的名称
        // os.version 操作系统的版本
        // file.separator 文件分隔符（在 UNIX 系统中是“/”）
        // path.separator 路径分隔符（在 UNIX 系统中是“:”）
        // line.separator 行分隔符（在 UNIX 系统中是“/n”）

        for (Object obj : properties.keySet()) {// properties:size =54
            System.out.println(obj.toString() + "-->" + properties.getProperty(obj.toString()));
        }

    }

}
